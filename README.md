Description
===========

This liferay hook adds support for authentication/registration based on OpenID Connect, which is e.g. used by Google.

Supported Liferay versions
--------------------------

The hook currently suports Liferay 6.2.x.

Usage
-----

Simply deploy the war into Liferay.

This will make a new authentication sub-page available under portal settings.

Redirect
--------

The login hook supports a `redirect` parameter. The value is stored in the user's session and he will be redirected to this URL after a successful login.

Configuration
-------------

Configuration can be performed under `portal settings / authentication` or through `portal-ext.properties`

Available properties are:

- `openidconnect.enabled` - Enables OpenID Connect authentication. Defaults to `false`
- `openidconnect.display` - Mode in which to open the authorization server's login URL. Valid values are
  - `page` - The authorization server's login page replaces the portal's page (through a redirect). After login the user is redirected back to the page displayed before the login attempt.
  - `popup` -  The authorization server's login page is dispalyed in a pop-up window.
  - `touch` - Same as `page` with an indication to the authorization server, that it should generate a UI which can easily be used on a touch-enabled device.
  - `wap` - Same as `page` with an indication to the authorization server, that it should generate a UI which can easily be used on a "feature-phone" device.
  Defaults to `popup`.
- `openidconnect.issuer.id` - Expected ID of the issuer. Defaults to `accounts.google.com`
- `openidconnect.issuer.name` - Name of the issuer to show on the login page. Defaults to `OpenID Connect`
- `openidconnect.issuer.logo.url` - Icon to be displayed for the issuer in the login page.
  If it contains no slash, it is interpreted as a Liferay icon. Defaults to `icon-google-plus-sign`.
- `openidconnect.client.id` - Client ID to be used for authentication requests against the IDP.
- `openidconnect.client.secret` - Client secret to be used for authentication requests against the IDP.
- `openidconnect.auth.url` - URL of the authentication endpoint. Defaults to `https://accounts.google.com/o/oauth2/auth`
- `openidconnect.token.url` - URL of the token endpoint. Defaults to `https://accounts.google.com/o/oauth2/token`
- `openidconnect.graph.url` - URL of the graph/userinfo endpoint. Defaults to `https://www.googleapis.com/oauth2/v2/userinfo`
- `openidconnect.scope` - Comma-separated list of OpenID Connect scopes to request. Defaults to `openid,profile,email`
- `openidconnect.revoke.url` - Revoke endpoint (complying to RFC 7009) to be used.
  If unset, revokations will not be performed. Defaults to `https://accounts.google.com/o/oauth2/revoke`
- `openidconnect.verified.account.required` - If set to `true`, user's for which the IDP does not declare the email address as
  verified will not be allowed to register or sign in. Defaults to `false`.
- `openidconnect.nonce.enabled` - Enables (recommended) nonce support in the authentication client.
  This may have to be disabled for providers (like Google) which give an error message if nonce support is turned on. Defaults to `false`.
- `openidconnect.userinfo.screenname.property` - Attribute from user-info which should be used to set the login name of a created user. If unset or if the attribute is not available in user-info, the screen name will be automatically generated. Defaut is unset.
- `openidconnect.userinfo.firstname.auto` - If set to `true`, a new user's first name will be set to the screen name. This requires the screen name property to be set. Defaults to `false`.
