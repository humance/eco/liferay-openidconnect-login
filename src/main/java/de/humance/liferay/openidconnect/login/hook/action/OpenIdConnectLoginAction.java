package de.humance.liferay.openidconnect.login.hook.action;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletMode;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.auth.openidconnect.IdToken;
import com.google.api.client.auth.openidconnect.IdTokenVerifier;
import com.google.api.client.http.BasicAuthentication;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroupRole;
import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

import de.humance.liferay.openidconnect.login.util.CredentialAndIdToken;
import de.humance.liferay.openidconnect.login.util.OpenIdConnectUtil;
import de.humance.liferay.openidconnect.login.util.WebKeys;
import de.humance.liferay.openidconnect.model.UserInfo;

public class OpenIdConnectLoginAction extends BaseStrutsAction {
	private static final String EXPANDO_OPEN_ID_CONNECT_SUBJECT = "openIdConnectSubject";
	private static final String EXPANDO_OPEN_ID_CONNECT_ISSUER = "openIdConnectIssuer";
	private static final String EXPANDO_OPEN_ID_CONNECT_ACCESS_TOKEN = "openIdConnectAccessToken";
	private static final String REDIRECT_URI = "/portal/openidconnect_login?cmd=token";

	private final Log log = LogFactoryUtil
			.getLog(OpenIdConnectLoginAction.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);

		final String cmd = ParamUtil.getString(request, Constants.CMD);

		if (cmd.equals("login")) {
			// Check for a redirect parameter
			String redirect = ParamUtil.getString(request, "redirect");
			if (Validator.isNotNull(redirect)) {
				request.getSession().setAttribute(WebKeys.REDIRECT, redirect);
			}

			AuthorizationCodeFlow codeFlow = getAuthorizationCodeFlow(themeDisplay
					.getCompanyId());

			SecureRandom rnd = new SecureRandom();

			String state = new BigInteger(130, rnd).toString(32);

			request.getSession().setAttribute(WebKeys.OPENID_CONNECT_STATE,
					state);

			AuthorizationCodeRequestUrl authorizationCodeRequestUrl = codeFlow
					.newAuthorizationUrl()
					.setRedirectUri(getRedirectURI(request))
					.setState(state)
					.set("display",
							OpenIdConnectUtil.getDisplay(themeDisplay
									.getCompanyId()))
					.set("ui_locales", getUiLocales(request));

			if (OpenIdConnectUtil.isNonceEnabled(themeDisplay.getCompanyId())) {
				String nonce = new BigInteger(130, rnd).toString(32);
				authorizationCodeRequestUrl.set("nonce", nonce);
				request.getSession().setAttribute(WebKeys.OPENID_CONNECT_NONCE,
						nonce);
			}

			log.debug(String.format(
					"%s / Redirecting to OpenID Connect provider", state));

			response.sendRedirect(authorizationCodeRequestUrl.build());
		} else if (cmd.equals("token")) {
			final HttpSession session = request.getSession();

			final String expectedState = GetterUtil.getString(session
					.getAttribute(WebKeys.OPENID_CONNECT_STATE));

			session.removeAttribute(WebKeys.OPENID_CONNECT_STATE);

			final String code = ParamUtil.getString(request, "code");
			final String state = ParamUtil.getString(request, "state");

			if (Validator.isNull(state) || !state.equals(expectedState)) {
				log.warn(String
						.format("%s / Received invalid state '%s' in authorization callback. Redirecting to login.",
								expectedState, state));
				// FIXME Set error message
				sendLoginRedirect(request, response);
				return null;
			}

			String expectedNonce = null;

			if (OpenIdConnectUtil.isNonceEnabled(themeDisplay.getCompanyId())) {
				expectedNonce = GetterUtil.getString(session
						.getAttribute(WebKeys.OPENID_CONNECT_NONCE));

				session.removeAttribute(WebKeys.OPENID_CONNECT_NONCE);

				if (Validator.isNull(expectedNonce)) {
					log.warn(state
							+ " / No Nonce value found in HTTP session. Redirecting to login.");
					sendLoginRedirect(request, response);
					return null;
				}
			}

			if (Validator.isNotNull(code)) {
				log.debug(String.format("%s / Retrieving access & id tokens",
						state));
				CredentialAndIdToken credentialAndId = getCredential(
						themeDisplay.getCompanyId(), code,
						getRedirectURI(request), expectedNonce);

				User user = setCredential(state, session,
						themeDisplay.getCompanyId(), credentialAndId);

				if (user != null
						&& (user.getStatus() == WorkflowConstants.STATUS_INCOMPLETE)) {
					log.debug(String
							.format("%s / User incomplete. Redirecting to update-account page.",
									state));
					sendUpdateAccountRedirect(request, response, user);

					return null;
				}

				sendLoginRedirect(request, response);

				return null;
			}

			String error = ParamUtil.getString(request, "error");

			if (error.equals("access_denied")) {
				SessionErrors.add(request, AuthException.class);
				sendLoginRedirect(request, response);

				return null;
			}
		}

		return null;
	}

	private String getUiLocales(HttpServletRequest request) {
		Locale primaryLocale = PortalUtil.getLocale(request);

		Locale availableLocales[] = LanguageUtil.getAvailableLocales();

		List<String> uiLocales = new ArrayList<String>(availableLocales.length);
		uiLocales.add(primaryLocale.toLanguageTag());

		for (Locale locale : availableLocales) {
			if (locale.equals(primaryLocale))
				continue;
			uiLocales.add(locale.toLanguageTag());
		}

		return StringUtil.merge(uiLocales, StringPool.SPACE);
	}

	protected User addUser(HttpSession session, long companyId,
			UserInfo userInfo) throws Exception {

		long creatorUserId = 0;
		boolean autoPassword = true;
		String password1 = StringPool.BLANK;
		String password2 = StringPool.BLANK;

		String screenName = getScreenName(companyId, userInfo);
		boolean autoScreenName = Validator.isNull(screenName);

		String emailAddress = userInfo.getEmail();
		String openId = StringPool.BLANK;
		Locale locale = (Locale) GetterUtil.getObject(
				LanguageUtil.getLocale(userInfo.getLocale()),
				LocaleUtil.getDefault());
		String firstName = getFirstName(companyId, userInfo);
		String middleName = GetterUtil.getString(userInfo.getMiddleName());
		String lastName = userInfo.getFamilyName();
		int prefixId = 0;
		int suffixId = 0;
		boolean male = Validator.equals(userInfo.getGender(), "male");
		int birthdayMonth = Calendar.JANUARY;
		int birthdayDay = 1;
		int birthdayYear = 1970;
		String jobTitle = StringPool.BLANK;
		long[] groupIds = null;
		long[] organizationIds = null;
		long[] roleIds = null;
		long[] userGroupIds = null;
		boolean sendEmail = true;

		ServiceContext serviceContext = new ServiceContext();

		User user = UserLocalServiceUtil.addUser(creatorUserId, companyId,
				autoPassword, password1, password2, autoScreenName, screenName,
				emailAddress, 0, openId, locale, firstName, middleName,
				lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay,
				birthdayYear, jobTitle, groupIds, organizationIds, roleIds,
				userGroupIds, sendEmail, serviceContext);

		user = UserLocalServiceUtil.updateLastLogin(user.getUserId(),
				user.getLoginIP());

		user = UserLocalServiceUtil
				.updatePasswordReset(user.getUserId(), false);

		user = UserLocalServiceUtil.updateEmailAddressVerified(
				user.getUserId(),
				GetterUtil.getBoolean(userInfo.getVerifiedEmail(), true));

		session.setAttribute(WebKeys.OPENID_CONNECT_USER_EMAIL_ADDRESS,
				emailAddress);

		return user;
	}

	private String getScreenName(long companyId, UserInfo userInfo)
			throws AuthException, SystemException {
		final String screenName;

		if (Validator.isNotNull(OpenIdConnectUtil
				.getUserInfoScreenNameProperty(companyId))) {
			if (!userInfo.containsKey(OpenIdConnectUtil
					.getUserInfoScreenNameProperty(companyId))) {
				throw new AuthException(
						"No "
								+ OpenIdConnectUtil
										.getUserInfoScreenNameProperty(companyId)
								+ " property received in userinfo");
			}
			screenName = GetterUtil.getString(userInfo.get(OpenIdConnectUtil
					.getUserInfoScreenNameProperty(companyId)));
		} else {
			screenName = StringPool.BLANK;
		}
		return screenName;
	}

	private String getFirstName(long companyId, UserInfo userInfo)
			throws SystemException, AuthException {
		String firstName = userInfo.getGivenName();

		if (Validator.isNull(firstName)
				&& OpenIdConnectUtil.isUserInfoFirstNameAuto(companyId)) {
			if (Validator.isNull(OpenIdConnectUtil
					.getUserInfoScreenNameProperty(companyId)))
				throw new SystemException(
						"Config error. If first name is to be auto-generated, the userinfo screenname property must be set in configuration.");
			firstName = GetterUtil.getString(userInfo.get(OpenIdConnectUtil
					.getUserInfoScreenNameProperty(companyId)));

			if (Validator.isNull(firstName)) {
				throw new AuthException(
						"First name is to be auto-generated, but no "
								+ OpenIdConnectUtil
										.getUserInfoScreenNameProperty(companyId)
								+ " received in userinfo");
			}
		}

		return firstName;
	}

	protected CredentialAndIdToken getCredential(long companyId,
			String authorizationCode, String redirectURI, String expectedNonce)
			throws Exception {

		AuthorizationCodeFlow codeFlow = getAuthorizationCodeFlow(companyId);

		AuthorizationCodeTokenRequest tokenRequest = codeFlow
				.newTokenRequest(authorizationCode);

		tokenRequest.setRedirectUri(redirectURI);

		TokenResponse tokenResponse = tokenRequest.execute();

		String error = (String) tokenResponse.get("error");

		if (Validator.isNotNull(error)) {
			throw new AuthException(error);
		}

		String idTokenStr = (String) tokenResponse.get("id_token");

		if (Validator.isNull(idTokenStr)) {
			// TODO Should OpenIdConnectUtil.revokeAccess()
			throw new AuthException("No id_token received");
		}

		IdToken idToken = IdToken.parse(tokenRequest.getJsonFactory(),
				idTokenStr);

		String expectedIssuer = OpenIdConnectUtil.getIssuerId(companyId);
		List<String> expectedAudience = Arrays.asList(OpenIdConnectUtil
				.getClientId(companyId));
		IdTokenVerifier verifier = new IdTokenVerifier.Builder()
				.setIssuer(expectedIssuer).setAudience(expectedAudience)
				.build();

		if (!verifier.verify(idToken)) {
			// TODO Should OpenIdConnectUtil.revokeAccess()
			
			if (!idToken.verifyIssuer(verifier.getIssuer())) {
				throw new AuthException("Invalid id_token / Issuer verification failed: " + idToken.getPayload().getIssuer() + " != " + verifier.getIssuer());
			}
			
			if (!idToken.verifyAudience(verifier.getAudience())) {
				throw new AuthException("Invalid id_token / Audience verification failed: " + idToken.getPayload().getAudience() + " != " + verifier.getAudience());
			}
			
			if (!idToken.verifyIssuedAtTime(verifier.getClock().currentTimeMillis(), verifier.getAcceptableTimeSkewSeconds())) {
				throw new AuthException("Invalid id_token / IssuedAt time skew > " + verifier.getAcceptableTimeSkewSeconds() + "s: "
						+ idToken.getPayload().getIssuedAtTimeSeconds() + " vs. local " + verifier.getClock().currentTimeMillis() / 1000);
			}
			
			if (!idToken.verifyExpirationTime(verifier.getClock().currentTimeMillis(), verifier.getAcceptableTimeSkewSeconds())) {
				throw new AuthException("Invalid id_token / Expiration time skew > " + verifier.getAcceptableTimeSkewSeconds() + "s: "
						+ idToken.getPayload().getExpirationTimeSeconds() + " vs. local " + verifier.getClock().currentTimeMillis() / 1000);
			}
			
			throw new AuthException("Invalid id_token");
		}

		if (expectedNonce != null
				&& !expectedNonce.equals(idToken.getPayload().getNonce())) {
			// TODO Should OpenIdConnectUtil.revokeAccess()
			throw new AuthException("id_token nonce mismatch");
		}

		return new CredentialAndIdToken(codeFlow.createAndStoreCredential(
				tokenResponse, null), idToken);
	}

	protected AuthorizationCodeFlow getAuthorizationCodeFlow(long companyId)
			throws Exception {

		final HttpTransport httpTransport = new NetHttpTransport();
		final JacksonFactory jsonFactory = new JacksonFactory();

		final String clientId = OpenIdConnectUtil.getClientId(companyId);
		final String clientSecret = OpenIdConnectUtil
				.getClientSecret(companyId);

		final GenericUrl tokenServerUrl = new GenericUrl(
				OpenIdConnectUtil.getAccessTokenURL(companyId));
		final String authorizationServerUrl = OpenIdConnectUtil
				.getAuthURL(companyId);

		return new AuthorizationCodeFlow.Builder(
				BearerToken.authorizationHeaderAccessMethod(), httpTransport,
				jsonFactory, tokenServerUrl, new BasicAuthentication(clientId,
						clientSecret), // new
										// ClientParametersAuthentication(clientId,
										// clientSecret),
				clientId, authorizationServerUrl).setScopes(
				OpenIdConnectUtil.getProfileScope(companyId)).build();
	}

	protected String getRedirectURI(HttpServletRequest request) {
		return PortalUtil.getPortalURL(request) + PortalUtil.getPathMain()
				+ REDIRECT_URI;
	}

	protected UserInfo getUserInfo(long companyId, Credential credentials)
			throws SystemException, IOException {
		return OpenIdConnectUtil.getGraphResource(
				OpenIdConnectUtil.getGraphURL(companyId), credentials,
				UserInfo.class);
	}

	protected void sendLoginRedirect(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		final ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);

		final String redirect = (String) request.getSession().getAttribute(
				WebKeys.REDIRECT);

		String loginRedirectUrl;
		if (OpenIdConnectUtil.isDisplayPopup(themeDisplay.getCompanyId())) {
			PortletURL portletURL = PortletURLFactoryUtil.create(request,
					PortletKeys.FAST_LOGIN, themeDisplay.getPlid(),
					PortletRequest.RENDER_PHASE);

			portletURL.setParameter("struts_action", "/login/login_redirect");
			portletURL.setWindowState(LiferayWindowState.POP_UP);

			if (Validator.isNotNull(redirect)) {
				portletURL.setParameter("redirect", redirect);
			}

			loginRedirectUrl = portletURL.toString();
		} else {
			loginRedirectUrl = themeDisplay.getURLSignIn();

			if (Validator.isNotNull(redirect)) {
				loginRedirectUrl = HttpUtil.setParameter(loginRedirectUrl,
						"redirect", redirect);
			}
		}

		response.sendRedirect(loginRedirectUrl);
	}

	protected void sendUpdateAccountRedirect(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);

		PortletURL portletURL = PortletURLFactoryUtil.create(request,
				PortletKeys.LOGIN, themeDisplay.getPlid(),
				PortletRequest.RENDER_PHASE);

		portletURL.setParameter("saveLastPath", Boolean.FALSE.toString());
		portletURL.setParameter("struts_action", "/login/update_account");

		PortletURL redirectURL = PortletURLFactoryUtil.create(request,
				PortletKeys.FAST_LOGIN, themeDisplay.getPlid(),
				PortletRequest.RENDER_PHASE);

		redirectURL.setParameter("struts_action", "/login/login_redirect");
		redirectURL.setParameter("emailAddress", user.getEmailAddress());
		redirectURL.setParameter("anonymousUser", Boolean.FALSE.toString());
		redirectURL.setPortletMode(PortletMode.VIEW);
		redirectURL.setWindowState(LiferayWindowState.POP_UP);

		portletURL.setParameter("redirect", redirectURL.toString());

		portletURL.setParameter("userId", String.valueOf(user.getUserId()));
		portletURL.setParameter("emailAddress", user.getEmailAddress());
		portletURL.setParameter("firstName", user.getFirstName());
		portletURL.setParameter("lastName", user.getLastName());
		portletURL.setPortletMode(PortletMode.VIEW);
		portletURL.setWindowState(LiferayWindowState.POP_UP);

		response.sendRedirect(portletURL.toString());
	}

	/**
	 * Retrieves (or creates) the Liferay user matching the given credentials.
	 * 
	 * Will search for an existing user using the ID token's issuer and subject
	 * values.
	 * 
	 * If no matching user is found, an email address lookup is performed. In
	 * this case, the existing user's OpenID information must match the values
	 * from the ID token.
	 * 
	 * If the user can still not be located, a new user is created based on the
	 * user info retrieved.
	 * 
	 * Existing users' information is updated with the user info values.
	 */
	protected User setCredential(String state, HttpSession session,
			long companyId, CredentialAndIdToken credentialAndId)
			throws Exception {
		UserInfo userInfo = getUserInfo(companyId,
				credentialAndId.getCredential());

		if (userInfo == null) {
			log.warn(String
					.format("%s / UserInfo could not be retrieved for %s:%s. Revoking access.",
							state, credentialAndId.getIdToken().getIssuer(),
							credentialAndId.getIdToken().getSubject()));

			OpenIdConnectUtil.revokeAccess(companyId,
					credentialAndId.getCredential());

			return null;
		}

		if (OpenIdConnectUtil.isVerifiedAccountRequired(companyId)
				&& !GetterUtil.getBoolean(userInfo.getVerifiedEmail())) {
			log.warn(String
					.format("%s / User with email address %s tried to login, but email address reported as not verified by OpenID provider %s. Revoking access.",
							state, userInfo.getEmail(), credentialAndId
									.getIdToken().getIssuer()));

			OpenIdConnectUtil.revokeAccess(companyId,
					credentialAndId.getCredential());

			return null;
		}

		User user = null;

		List<ExpandoValue> subjects = ExpandoValueLocalServiceUtil
				.getColumnValues(companyId, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME,
						EXPANDO_OPEN_ID_CONNECT_SUBJECT, credentialAndId
								.getIdToken().getSubject(), -1, -1);

		for (ExpandoValue subject : subjects) {
			String issuer = (String) ExpandoValueLocalServiceUtil.getData(
					companyId, User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME,
					EXPANDO_OPEN_ID_CONNECT_ISSUER, subject.getClassPK());

			if (credentialAndId.getIdToken().getIssuer().equals(issuer)) {
				user = UserLocalServiceUtil.getUserById(companyId,
						subject.getClassPK());
			}
		}

		final String emailAddress = userInfo.getEmail();

		if (user == null && Validator.isNotNull(emailAddress)) {
			user = UserLocalServiceUtil.fetchUserByEmailAddress(companyId,
					emailAddress);

			if (user != null) {
				// Check for mismatching issuer & subject
				String issuer = (String) ExpandoValueLocalServiceUtil.getData(
						companyId, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME,
						EXPANDO_OPEN_ID_CONNECT_ISSUER, user.getUserId());
				String subject = (String) ExpandoValueLocalServiceUtil.getData(
						companyId, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME,
						EXPANDO_OPEN_ID_CONNECT_SUBJECT, user.getUserId());

				if (Validator.isNotNull(issuer)
						&& Validator.isNotNull(subject)
						&& (!credentialAndId.getIdToken().getIssuer()
								.equals(issuer) || !credentialAndId
								.getIdToken().getSubject().equals(subject))) {
					log.error(String
							.format("%s / User with email address %s tried to use different OpenID identity %s:%s (expected %s:%s). Revoking access.",
									state, emailAddress, credentialAndId
											.getIdToken().getIssuer(),
									credentialAndId.getIdToken().getSubject(),
									issuer, subject));

					OpenIdConnectUtil.revokeAccess(companyId,
							credentialAndId.getCredential());

					return null;
				}
			}
		}

		// User found by issuer & subject or email address
		if (user != null
				&& user.getStatus() != WorkflowConstants.STATUS_INCOMPLETE) {

			session.setAttribute(WebKeys.OPENID_CONNECT_USER_EMAIL_ADDRESS,
					emailAddress);
		}

		if (user != null) {
			if (user.getStatus() == WorkflowConstants.STATUS_INCOMPLETE) {
				session.setAttribute(WebKeys.OPENID_CONNECT_INCOMPLETE_USER_ID,
						userInfo.getSubject());

				user.setEmailAddress(userInfo.getEmail());
				user.setFirstName(getFirstName(companyId, userInfo));
				user.setLastName(userInfo.getFamilyName());

				return user;
			}

			log.debug(String.format("%s / Updating user %d", state,
					user.getUserId()));
			user = updateUser(user, userInfo);
		} else {
			log.debug(String.format("%s / Creating user %s", state,
					userInfo.getEmail()));
			user = addUser(session, companyId, userInfo);
		}

		updateExpandoValues(user, credentialAndId, userInfo);

		return user;
	}

	protected User updateUser(User user, UserInfo userInfo) throws Exception {
		String emailAddress = userInfo.getEmail();
		String screenName = getScreenName(user.getCompanyId(), userInfo);
		if (Validator.isNull(screenName)) {
			screenName = user.getScreenName();
		}

		String firstName = getFirstName(user.getCompanyId(), userInfo);
		String middleName = GetterUtil.getString(userInfo.getMiddleName());
		String lastName = userInfo.getFamilyName();
		boolean male = Validator.equals(userInfo.getGender(), "male");

		if (emailAddress.equals(user.getEmailAddress())
				&& firstName.equals(user.getFirstName())
				&& lastName.equals(user.getLastName())
				&& middleName.equals(user.getMiddleName())
				&& (male == user.isMale())) {

			return user;
		}

		Contact contact = user.getContact();

		Calendar birthdayCal = CalendarFactoryUtil.getCalendar();

		birthdayCal.setTime(contact.getBirthday());

		int birthdayMonth = birthdayCal.get(Calendar.MONTH);
		int birthdayDay = birthdayCal.get(Calendar.DAY_OF_MONTH);
		int birthdayYear = birthdayCal.get(Calendar.YEAR);

		long[] groupIds = null;
		long[] organizationIds = null;
		long[] roleIds = null;
		List<UserGroupRole> userGroupRoles = null;
		long[] userGroupIds = null;

		ServiceContext serviceContext = new ServiceContext();

		if (!StringUtil.equalsIgnoreCase(emailAddress, user.getEmailAddress())) {

			UserLocalServiceUtil.updateEmailAddress(user.getUserId(),
					StringPool.BLANK, emailAddress, emailAddress);
		}

		user = UserLocalServiceUtil.updateEmailAddressVerified(
				user.getUserId(),
				GetterUtil.getBoolean(userInfo.getVerifiedEmail(), true));

		return UserLocalServiceUtil.updateUser(user.getUserId(),
				StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, false,
				user.getReminderQueryQuestion(), user.getReminderQueryAnswer(),
				screenName, emailAddress, 0, user.getOpenId(),
				user.getLanguageId(), user.getTimeZoneId(), user.getGreeting(),
				user.getComments(), firstName, user.getMiddleName(), lastName,
				contact.getPrefixId(), contact.getSuffixId(), male,
				birthdayMonth, birthdayDay, birthdayYear, contact.getSmsSn(),
				contact.getAimSn(), contact.getFacebookSn(),
				contact.getIcqSn(), contact.getJabberSn(), contact.getMsnSn(),
				contact.getMySpaceSn(), contact.getSkypeSn(),
				contact.getTwitterSn(), contact.getYmSn(),
				contact.getJobTitle(), groupIds, organizationIds, roleIds,
				userGroupRoles, userGroupIds, serviceContext);
	}

	protected void updateExpandoValues(User user,
			CredentialAndIdToken credentialAndId, UserInfo userinfo)
			throws Exception {
		ExpandoValueLocalServiceUtil.addValue(user.getCompanyId(),
				User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
				EXPANDO_OPEN_ID_CONNECT_ISSUER, user.getUserId(),
				credentialAndId.getIdToken().getIssuer());
		ExpandoValueLocalServiceUtil.addValue(user.getCompanyId(),
				User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
				EXPANDO_OPEN_ID_CONNECT_SUBJECT, user.getUserId(),
				credentialAndId.getIdToken().getSubject());
		ExpandoValueLocalServiceUtil.addValue(user.getCompanyId(),
				User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
				EXPANDO_OPEN_ID_CONNECT_ACCESS_TOKEN, user.getUserId(),
				credentialAndId.getCredential().getAccessToken());
	}
}