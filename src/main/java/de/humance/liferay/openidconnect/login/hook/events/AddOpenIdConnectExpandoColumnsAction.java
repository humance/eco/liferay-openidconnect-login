package de.humance.liferay.openidconnect.login.hook.events;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.model.User;
import com.liferay.portlet.expando.NoSuchTableException;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoColumnConstants;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;

public class AddOpenIdConnectExpandoColumnsAction extends SimpleAction {
	@Override
	public void run(String[] ids) throws ActionException {
		try {
			doRun(GetterUtil.getLong(ids[0]));
		} catch (Exception e) {
			throw new ActionException(e);
		}
	}

	protected void addExpandoColumn(ExpandoTable expandoTable, String name,
			UnicodeProperties properties) throws Exception {

		ExpandoColumn expandoColumn = ExpandoColumnLocalServiceUtil.getColumn(
				expandoTable.getTableId(), name);

		if (expandoColumn != null) {
			return;
		}

		expandoColumn = ExpandoColumnLocalServiceUtil.addColumn(
				expandoTable.getTableId(), name, ExpandoColumnConstants.STRING);

		ExpandoColumnLocalServiceUtil.updateTypeSettings(
				expandoColumn.getColumnId(), properties.toString());
	}

	protected void doRun(long companyId) throws Exception {
		ExpandoTable expandoTable = null;

		try {
			expandoTable = ExpandoTableLocalServiceUtil.getTable(companyId,
					User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME);
		} catch (NoSuchTableException e) {
			expandoTable = ExpandoTableLocalServiceUtil.addTable(companyId,
					User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME);
		}

		UnicodeProperties properties = new UnicodeProperties();

		properties.setProperty("hidden", "true");
		properties.setProperty("visible-with-update-permission", "false");

		addExpandoColumn(expandoTable, "openIdConnectIssuer", properties);
		addExpandoColumn(expandoTable, "openIdConnectSubject", properties);
		addExpandoColumn(expandoTable, "openIdConnectAccessToken", properties);
	}
}
