package de.humance.liferay.openidconnect.login.hook.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

import de.humance.liferay.openidconnect.login.util.OpenIdConnectUtil;

public class OpenIdConnectPreLogoutAction extends Action {
	private static Log log = LogFactoryUtil
			.getLog(OpenIdConnectPreLogoutAction.class);

	@Override
	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException {
		User user = null;
		try {
			user = PortalUtil.getUser(request);
		} catch (Exception e) {
			log.error("Could not get current User from HttpServletRequest", e);
		}
		if (user == null) {
			return;
		}

		ExpandoValue value = null;
		try {
			value = ExpandoValueLocalServiceUtil.getValue(user.getCompanyId(),
					User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME,
					"openIdConnectAccessToken", user.getUserId());
		} catch (Exception e) {
			log.error(
					"Could not remove value of OpenId Connect Access Token Expando",
					e);
		}
		if (value == null) {
			return;
		}

		try {
			OpenIdConnectUtil
					.revokeAccess(user.getCompanyId(), value.getData());
		} catch (Exception e) {
			log.error(
					"Could not revoke AccessToken for User with id "
							+ user.getUserId(), e);
		}

		try {
			ExpandoValueLocalServiceUtil.deleteValue(value);
		} catch (Exception e) {
			log.error(
					"Could not remove value of OpenId Connect Access Token Expando",
					e);
		}

	}
}
