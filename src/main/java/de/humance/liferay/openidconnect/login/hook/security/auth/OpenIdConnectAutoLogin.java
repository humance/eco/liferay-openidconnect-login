package de.humance.liferay.openidconnect.login.hook.security.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.BaseAutoLogin;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import de.humance.liferay.openidconnect.login.util.OpenIdConnectUtil;
import de.humance.liferay.openidconnect.login.util.WebKeys;

public class OpenIdConnectAutoLogin extends BaseAutoLogin {
	@Override
	protected String[] doLogin(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		long companyId = PortalUtil.getCompanyId(request);

		if (!OpenIdConnectUtil.isEnabled(companyId)) {
			return null;
		}

		User user = getUser(request, companyId);

		if (user == null) {
			return null;
		}

		String[] credentials = new String[3];

		credentials[0] = String.valueOf(user.getUserId());
		credentials[1] = user.getPassword();
		credentials[2] = Boolean.TRUE.toString();

		return credentials;
	}

	protected User getUser(HttpServletRequest request, long companyId)
			throws Exception {

		HttpSession session = request.getSession();

		String emailAddress = GetterUtil.getString(session
				.getAttribute(WebKeys.OPENID_CONNECT_USER_EMAIL_ADDRESS));

		if (Validator.isNull(emailAddress)) {
			return null;
		}

		session.removeAttribute(WebKeys.OPENID_CONNECT_USER_EMAIL_ADDRESS);

		User user = UserLocalServiceUtil.getUserByEmailAddress(companyId,
				emailAddress);

		return user;
	}
}