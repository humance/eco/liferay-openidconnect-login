package de.humance.liferay.openidconnect.login.util;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.openidconnect.IdToken;

public class CredentialAndIdToken {
	private Credential credential;
	private IdToken.Payload idToken;

	public CredentialAndIdToken(Credential credential, IdToken idToken) {
		this.credential = credential;
		this.idToken = idToken.getPayload();
	}

	public Credential getCredential() {
		return credential;
	}

	public IdToken.Payload getIdToken() {
		return idToken;
	}
}
