package de.humance.liferay.openidconnect.login.util;

import java.io.IOException;
import java.util.Collection;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

public class OpenIdConnectUtil {
	public static String getIssuerId(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.ISSUER_ID);
	}

	public static String getIssuerName(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.ISSUER_NAME);
	}

	public static String getIssuerLogoUrl(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.ISSUER_LOGO_URL);
	}

	public static String getAccessTokenURL(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.TOKEN_URL);
	}

	public static String getClientId(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.CLIENT_ID);
	}

	public static String getClientSecret(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.CLIENT_SECRET);
	}

	public static String getAuthURL(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.AUTH_URL);
	}

	public static <T extends GenericJson> T getGraphResource(String url,
			Credential credentials, Class<T> dataClass) throws SystemException,
			IOException {
		T result = credentials
				.getTransport()
				.createRequestFactory(credentials)
				.buildGetRequest(new GenericUrl(url))
				.setParser(
						credentials.getJsonFactory().createJsonObjectParser())
				.execute().parseAs(dataClass);
		return result;
	}

	public static String getGraphURL(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.GRAPH_URL);
	}

	public static Collection<String> getProfileScope(long companyId)
			throws SystemException {
		String[] stringArray = PrefsPropsUtil.getStringArray(companyId,
				OpenIdConnectPropsKeys.AUTH_SCOPE, StringPool.COMMA);

		return ListUtil.fromArray(stringArray);
	}

	public static boolean isEnabled(long companyId) throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.ENABLED);
	}

	public static boolean isVerifiedAccountRequired(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.VERIFIED_ACCOUNT_REQUIRED);
	}

	public static boolean isNonceEnabled(long companyId) throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.NONCE_ENABLED);
	}

	public static String getDisplay(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.DISPLAY);
	}

	public static boolean isDisplayPopup(long companyId) throws SystemException {
		return "popup".equals(getDisplay(companyId));
	}

	public static String getUserInfoScreenNameProperty(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.USERINFO_SCREEN_NAME_PROPERTY);
	}

	public static boolean isUserInfoFirstNameAuto(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.USERINFO_FIRST_NAME_AUTO);
	}

	public static String getRevokeURL(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.REVOKE_URL);
	}

	public static void revokeAccess(long companyId, String accessToken)
			throws SystemException, IOException {
		Credential cred = new Credential.Builder(
				BearerToken.authorizationHeaderAccessMethod())
				.setTransport(new NetHttpTransport())
				.setJsonFactory(new JacksonFactory()).build()
				.setAccessToken(accessToken);
		revokeAccess(companyId, cred);
	}

	public static void revokeAccess(long companyId, Credential credentials)
			throws SystemException, IOException {
		String revokeUrlStr = getRevokeURL(companyId);

		if (Validator.isNull(revokeUrlStr))
			return;

		GenericUrl revokeUrl = new GenericUrl(revokeUrlStr);
		revokeUrl.set("token", credentials.getAccessToken());

		credentials
				.getTransport()
				.createRequestFactory(credentials)
				.buildGetRequest(revokeUrl)
				.setParser(
						credentials.getJsonFactory().createJsonObjectParser())
				.execute();
	}
}
