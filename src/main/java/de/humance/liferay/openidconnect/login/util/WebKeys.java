package de.humance.liferay.openidconnect.login.util;

public class WebKeys implements com.liferay.portal.kernel.util.WebKeys {
	public static final String OPENID_CONNECT_INCOMPLETE_USER_ID = "OPENID_CONNECT_INCOMPLETE_USER_ID";
	public static final String OPENID_CONNECT_NONCE = "OPENID_CONNECT_NONCE";
	public static final String OPENID_CONNECT_STATE = "OPENID_CONNECT_STATE";
	public static final String OPENID_CONNECT_USER_EMAIL_ADDRESS = "OPENID_CONNECT_USER_EMAIL_ADDRESS";
	public static final String OPENID_CREDENTIAL = "OPENID_CREDENTIALS";
}