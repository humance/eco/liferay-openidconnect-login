package de.humance.liferay.openidconnect.model;

import com.google.api.client.json.GenericJson;

public class UserInfo extends GenericJson {
	/**
	 * The user's email address. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String email;
	
	/**
	 * The user's last name. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key("family_name")
	private String familyName;

	/**
	 * The user's gender. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String gender;

	/**
	 * The user's first name. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key("given_name")
	private String givenName;
	
	/**
	 * The user's middle name. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key("middle_name")
	private String middleName;

	/**
	 * The hosted domain e.g. example.com if the user is Google apps user. The
	 * value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String hd;

	/**
	 * The focus obfuscated gaia id of the user. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key("sub")
	private String subject;

	/**
	 * The user's default locale. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String locale;

	/**
	 * The user's full name. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String name;

	/**
	 * URL of the user's picture image. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String picture;

	/**
	 * The user's default timezone. The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private String timezone;

	/**
	 * Boolean flag which is true if the email address is verified. The value
	 * may be {@code null}.
	 */
	@com.google.api.client.util.Key("email_verified")
	private Boolean verifiedEmail;

	/**
	 * The user's email address.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * The user's email address.
	 * 
	 * @param email
	 *            email or {@code null} for none
	 */
	public UserInfo setEmail(String email) {
		this.email = email;
		return this;
	}

	/**
	 * The user's last name.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * The user's last name.
	 * 
	 * @param familyName
	 *            familyName or {@code null} for none
	 */
	public UserInfo setFamilyName(String familyName) {
		this.familyName = familyName;
		return this;
	}

	/**
	 * The user's gender.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * The user's gender.
	 * 
	 * @param gender
	 *            gender or {@code null} for none
	 */
	public UserInfo setGender(String gender) {
		this.gender = gender;
		return this;
	}

	/**
	 * The user's first name.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * The user's first name.
	 * 
	 * @param givenName
	 *            givenName or {@code null} for none
	 */
	public UserInfo setGivenName(String givenName) {
		this.givenName = givenName;
		return this;
	}

	/**
	 * The hosted domain e.g. example.com if the user is Google apps user.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getHd() {
		return hd;
	}

	/**
	 * The hosted domain e.g. example.com if the user is Google apps user.
	 * 
	 * @param hd
	 *            hd or {@code null} for none
	 */
	public UserInfo setHd(String hd) {
		this.hd = hd;
		return this;
	}

	/**
	 * The subject of the user.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * The focus obfuscated gaia id of the user.
	 * 
	 * @param id
	 *            id or {@code null} for none
	 */
	public UserInfo setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	/**
	 * The user's default locale.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * The user's default locale.
	 * 
	 * @param locale
	 *            locale or {@code null} for none
	 */
	public UserInfo setLocale(String locale) {
		this.locale = locale;
		return this;
	}

	/**
	 * The user's full name.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getName() {
		return name;
	}

	/**
	 * The user's full name.
	 * 
	 * @param name
	 *            name or {@code null} for none
	 */
	public UserInfo setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * URL of the user's picture image.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * URL of the user's picture image.
	 * 
	 * @param picture
	 *            picture or {@code null} for none
	 */
	public UserInfo setPicture(String picture) {
		this.picture = picture;
		return this;
	}

	/**
	 * The user's default timezone.
	 * 
	 * @return value or {@code null} for none
	 */
	public String getTimezone() {
		return timezone;
	}

	/**
	 * The user's default timezone.
	 * 
	 * @param timezone
	 *            timezone or {@code null} for none
	 */
	public UserInfo setTimezone(String timezone) {
		this.timezone = timezone;
		return this;
	}

	/**
	 * Boolean flag which is true if the email address is verified.
	 * 
	 * @return value or {@code null} for none
	 */
	public Boolean getVerifiedEmail() {
		return verifiedEmail;
	}

	/**
	 * Boolean flag which is true if the email address is verified.
	 * 
	 * @param verifiedEmail
	 *            verifiedEmail or {@code null} for none
	 */
	public UserInfo setVerifiedEmail(Boolean verifiedEmail) {
		this.verifiedEmail = verifiedEmail;
		return this;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Override
	public UserInfo set(String fieldName, Object value) {
		return (UserInfo) super.set(fieldName, value);
	}

	@Override
	public UserInfo clone() {
		return (UserInfo) super.clone();
	}
}
