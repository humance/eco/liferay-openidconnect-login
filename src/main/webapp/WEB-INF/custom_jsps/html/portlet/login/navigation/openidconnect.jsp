<%@ include file="/html/portlet/login/init.jsp" %>

<%!
// As the JSP cannot access OpenIdConnectPropsKeys, these have to be redefined here.
private static final String DISPLAY = "openidconnect.display";
private static final String ISSUER_NAME = "openidconnect.issuer.name";
private static final String ISSUER_LOGO_URL = "openidconnect.issuer.logo.url";
private static final String CLIENT_ID = "openidconnect.client.id";
private static final String CLIENT_SECRET = "openidconnect.client.secret";
private static final String ENABLED = "openidconnect.enabled";
%>

<%
long companyId = company.getCompanyId();
boolean openIdConnectEnabled = PrefsPropsUtil.getBoolean(companyId, ENABLED);
boolean openIdConnectDisplayPopup = "popup".equals(PrefsPropsUtil.getString(companyId, DISPLAY));
String openIdConnectClientId = PrefsPropsUtil.getString(companyId, CLIENT_ID);
String openIdConnectClientSecret = PrefsPropsUtil.getString(companyId, CLIENT_SECRET);
String openIdConnectIssuerName = PrefsPropsUtil.getString(companyId, ISSUER_NAME);
String openIdConnectIssuerLogoUrl = PrefsPropsUtil.getString(companyId, ISSUER_LOGO_URL, null);

if (Validator.isNull(openIdConnectClientId) || Validator.isNull(openIdConnectClientSecret)) {
	openIdConnectEnabled = false;
}

String providerIconCssClass = null;
String providerIconSrc = null;

if (openIdConnectIssuerLogoUrl != null && openIdConnectIssuerLogoUrl.contains(StringPool.SLASH)) {
	providerIconSrc = openIdConnectIssuerLogoUrl;
} else if (openIdConnectIssuerLogoUrl != null) {
	providerIconCssClass = openIdConnectIssuerLogoUrl;
}

String redirect = request.getParameter("redirect");
%>

<c:if test="<%= openIdConnectEnabled %>">

	<%
	String openIdConnectAuthURL = PortalUtil.getPathContext() + "/c/portal/openidconnect_login?cmd=login";
	
	if(Validator.isNotNull(redirect)) {
		openIdConnectAuthURL = openIdConnectAuthURL + "&redirect=" + redirect;
	}
	
	String openIdConnectLoginUrl;
	if (openIdConnectDisplayPopup) {
		openIdConnectLoginUrl = "javascript:var openIdConnectLoginWindow = window.open('" + openIdConnectAuthURL.toString() + "', '" + openIdConnectIssuerName + "', 'align=center,directories=no,height=560,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=1000'); void(''); openIdConnectLoginWindow.focus();";
	} else {
		openIdConnectLoginUrl = openIdConnectAuthURL.toString();
	}
	%>

	<liferay-ui:icon
		id="openid-connect-btn"
		src="<%= providerIconSrc %>"
		iconCssClass="<%= providerIconCssClass %>"
		message="<%= openIdConnectIssuerName %>"
		url="<%= openIdConnectLoginUrl %>"
	/>
</c:if>