<%@include file="/html/portlet/portal_settings/init.jsp" %>

<%!
// As the JSP cannot access OpenIdConnectPropsKeys, these have to be redefined here.
private static final String DISPLAY = "openidconnect.display";
private static final String ISSUER_ID = "openidconnect.issuer.id";
private static final String ISSUER_NAME = "openidconnect.issuer.name";
private static final String ISSUER_LOGO_URL = "openidconnect.issuer.logo.url";
private static final String CLIENT_ID = "openidconnect.client.id";
private static final String CLIENT_SECRET = "openidconnect.client.secret";
private static final String ENABLED = "openidconnect.enabled";
private static final String GRAPH_URL = "openidconnect.graph.url";
private static final String USERINFO_SCREEN_NAME_PROPERTY = "openidconnect.userinfo.screenname.property";
private static final String USERINFO_FIRST_NAME_AUTO = "openidconnect.userinfo.firstname.auto";
private static final String AUTH_URL = "openidconnect.auth.url";
private static final String TOKEN_URL = "openidconnect.token.url";
private static final String VERIFIED_ACCOUNT_REQUIRED = "openidconnect.verified.account.required";
private static final String REVOKE_URL = "openidconnect.revoke.url";
private static final String NONCE_ENABLED = "openidconnect.nonce.enabled";
%>

<%
long companyId = company.getCompanyId();

boolean openIdConnectEnabled = PrefsPropsUtil.getBoolean(companyId, ENABLED);
boolean openIdConnectVerifiedAccountRequired = PrefsPropsUtil.getBoolean(companyId, VERIFIED_ACCOUNT_REQUIRED);
boolean openIdConnectNonceEnabled = PrefsPropsUtil.getBoolean(companyId, NONCE_ENABLED);
String openIdConnectDisplay = PrefsPropsUtil.getString(companyId, DISPLAY);
String openIdConnectIssuerId = PrefsPropsUtil.getString(companyId, ISSUER_ID);
String openIdConnectIssuerName = PrefsPropsUtil.getString(companyId, ISSUER_NAME);
String openIdConnectIssuerLogoUrl = PrefsPropsUtil.getString(companyId, ISSUER_LOGO_URL);
String openIdConnectClientId = PrefsPropsUtil.getString(companyId, CLIENT_ID);
String openIdConnectClientSecret = PrefsPropsUtil.getString(companyId, CLIENT_SECRET);
String openIdConnectUserInfoURL = PrefsPropsUtil.getString(companyId, GRAPH_URL);
String openIdConnectAuthURL = PrefsPropsUtil.getString(companyId, AUTH_URL);
String openIdConnectTokenURL = PrefsPropsUtil.getString(companyId, TOKEN_URL);
String openIdConnectRevokeURL = PrefsPropsUtil.getString(companyId, REVOKE_URL);
String openIdConnectUserInfoScreenNameProperty = PrefsPropsUtil.getString(companyId, USERINFO_SCREEN_NAME_PROPERTY);
boolean openIdConnectUserInfoFirstNameAuto = PrefsPropsUtil.getBoolean(companyId, USERINFO_FIRST_NAME_AUTO);
%>

<aui:row>
	<aui:col width="<%= 50 %>">
		<aui:input inlineLabel="left" label="enabled" name='<%= "settings--" + ENABLED + "--" %>' type="checkbox" value="<%= openIdConnectEnabled %>" />
	</aui:col>
	<aui:col width="<%= 50 %>">
		<aui:select label="display" name='<%= "settings--" + DISPLAY + "--" %>' helpMessage="openidconnect-display-help">
			<aui:option label="page" value="page" selected='<%= openIdConnectDisplay.equals("page") %>' />
			<aui:option label="pop-up" value="popup" selected='<%= openIdConnectDisplay.equals("popup") %>' />
		</aui:select>
	</aui:col>
</aui:row>

<aui:row>
	<aui:col width="<%= 50 %>">
		<aui:input label="openidconnect-client-id" name='<%= "settings--" + CLIENT_ID + "--" %>' type="text" value="<%= openIdConnectClientId %>" />
	</aui:col>
	<aui:col width="<%= 50 %>">
		<aui:input label="openidconnect-client-secret" name='<%= "settings--" + CLIENT_SECRET + "--" %>' type="text" value="<%= openIdConnectClientSecret %>" />
	</aui:col>
</aui:row>

<h3><liferay-ui:message key="Identity Provider" /></h3>
<aui:row>
	<aui:col width="<%= 50 %>">
		<aui:input label="openidconnect-issuer-id" name='<%= "settings--" + ISSUER_ID + "--" %>' type="text" value="<%= openIdConnectIssuerId %>" />
		<aui:input label="openidconnect-issuer-name" name='<%= "settings--" + ISSUER_NAME + "--" %>' type="text" value="<%= openIdConnectIssuerName %>" />
		<aui:input label="openidconnect-issuer-logo-url" name='<%= "settings--" + ISSUER_LOGO_URL + "--" %>' type="text" value="<%= openIdConnectIssuerLogoUrl %>" />
		<aui:input inlineLabel="left" label="openidconnect-nonce-enabled" name='<%= "settings--" + NONCE_ENABLED + "--" %>' type="checkbox" value="<%= openIdConnectNonceEnabled %>" />
	</aui:col>
	<aui:col width="<%= 50 %>">
		<aui:input label="openidconnect-authentication-url" name='<%= "settings--" + AUTH_URL + "--" %>' type="text" value="<%= openIdConnectAuthURL %>" />
		<aui:input label="openidconnect-token-url" name='<%= "settings--" + TOKEN_URL + "--" %>' type="text" value="<%= openIdConnectTokenURL %>" />
		<aui:input label="openidconnect-revoke-url" name='<%= "settings--" + REVOKE_URL + "--" %>' type="text" value="<%= openIdConnectRevokeURL %>" />
		<aui:input label="openidconnect-userinfo-url" name='<%= "settings--" + GRAPH_URL + "--" %>' type="text" value="<%= openIdConnectUserInfoURL %>" />
	</aui:col>
</aui:row>

<h3><liferay-ui:message key="User creation" /></h3>
<aui:row>
	<aui:col width="<%= 50 %>">
		<aui:input inlineLabel="left" label="verified-account-required" name='<%= "settings--" + VERIFIED_ACCOUNT_REQUIRED + "--" %>' type="checkbox" value="<%= openIdConnectVerifiedAccountRequired %>" />
		<aui:input label="openidconnect-userinfo-screenname-property" name='<%= "settings--" + USERINFO_SCREEN_NAME_PROPERTY + "--" %>' type="text" value="<%= openIdConnectUserInfoScreenNameProperty %>" helpMessage="openidconnect-userinfo-screenname-property-help" />
		<aui:input inlineLabel="left" label="openidconnect-userinfo-firstname-auto" name='<%= "settings--" + USERINFO_FIRST_NAME_AUTO + "--" %>' type="checkbox" value="<%= openIdConnectUserInfoFirstNameAuto %>" helpMessage="openidconnect-userinfo-firstname-auto-help" />
	</aui:col>
</aui:row>